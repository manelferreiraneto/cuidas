/* General dependencies */
require('dotenv').config()
const express = require('express');
const logger = require('morgan');
const cors = require('cors');

/* Import database */
const database = require('./database');

/* Setup NodeJS app */
const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(cors());

/* Connect to database */
database.connect().then(() => {
  console.log('Connected on database');
});

/* Setup routes */
const routesBase = require('./routes/base');
routesBase(app);

// catch 404 and forward to error handler
// app.use((req, res, next) => {
//   next(createError(404));
// });

module.exports = app;
