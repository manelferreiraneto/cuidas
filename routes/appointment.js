const express = require('express');
const { checkSchema, validationResult } = require('express-validator');
const moment = require('moment');
const router = express.Router();

const models = require('../models/base');
const { Patient, Appointment } = models();

router.post('/', checkSchema({
  fullname: { matches: /^([a-zA-ZÀ-ŸẽẼ]+)\s{1,}([a-zA-ZÀ-ŸẽẼ]\s*)+$/ },
  email: { isEmail: true, isEmpty: false },
  phoneNumber: { matches: /^\d{10,11}$/, isString: true },
  details: { isString: true, optional: true },
  date: { 
    isEmpty: false,
    custom: {
      options: (value) => {
        return moment(value, 'YYYY-MM-DD HH:mm', true).isValid() && 
          moment(value, 'YYYY-MM-DD HH:mm').isAfter(moment())
      },
      errorMessage: 'Check if date is valid (YYYY-MM-DD HH:mm) and is in the future'
    }
  },
  duration: {
    isEmpty: false,
    isInt: true,
    custom: {
      options: (value) => (value >= 10 && value <= 30) && (value % 10 === 0),
      errorMessage: 'Appointment duration must be between 10 and 30 minutes, always in 10 minutes intervals.'
    }
  }
}), async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(400).json({ errors: errors.array() });

    const {
      fullname,
      email,
      phoneNumber,
      details,
      date,
      duration
    } = req.body;

    const patient = await Patient.findOrCreatePatient(
      fullname, 
      email, 
      phoneNumber
    );

    const appointment = await Appointment.createAppointment(
      patient._id,
      details,
      date,
      duration
    );

    return res.status(200).json({
      patient: patient,
      appointment: appointment
    });
  } catch (e) {
    return res.status(e.statusCode || 500)
      .json({ message: e.message });
  }
});

router.get('/time_availability', async (req, res, next) => {
  try {
    const availableTimes = await Appointment.getAvailableTimes();

    return res.status(200).json(availableTimes);
  } catch (e) {
    return res.status(e.statusCode || 500)
      .json({ message: e.stack });
  }
})

router.get('/', async(req, res, next) => {
  try {
    const appointments = await Appointment.getAppointments();

    return res.status(200).json(appointments);
  } catch (e) {
    return res.status(e.statusCode || 500)
      .json({ message: e.message });
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const {
      id
    } = req.params;

    const appointment = await Appointment.cancelAppointment(id);

    return res.status(200).json(appointment);
  } catch (e) {
    return res.status(e.statusCode || 500)
      .json({ message: e.message });
  }
});

module.exports = router;