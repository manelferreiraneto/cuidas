/* Require routers */
const appointmentRouter = require('./appointment');

module.exports = (app) => {
  app.use('/appointment', appointmentRouter);
}