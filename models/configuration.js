const mongoose = require('mongoose');
const _ = require('lodash');

const Schema = mongoose.Schema;

const configurationSchema = new Schema({
  name: {
    type: String,
    unique: true
  },
  value: {
    type: Schema.Types.Mixed
  }
});

const Configuration = mongoose.model('Configuration', configurationSchema, 'configuration');

module.exports = Configuration;