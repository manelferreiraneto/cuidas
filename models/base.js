const Patient = require('./patient');
const Appointment = require('./appointment');
const Configuration = require('./configuration');

module.exports = () => {
  const models = {
    Patient,
    Appointment,
    Configuration
  };

  return models;
}