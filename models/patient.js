const mongoose = require('mongoose');
const _ = require('lodash');

const patientSchema = new mongoose.Schema({
  fullname: {
    type: String,
    match: /^([a-zA-ZÀ-ŸẽẼ]+)\s{1,}([a-zA-ZÀ-ŸẽẼ]\s*)+$/
  },
  email: {
    type: String,
    match: /^([a-zA-Z0-9]+[\.\-\_]?[a-zA-Z0-9]+)@([a-z]+)(\.[a-z]+)+$/,
    unique: true
  },
  phoneNumber: {
    type: String,
    match: /^\d{10,11}$/
  }
});

patientSchema.pre('save', (next) => {
  this.fullname = _.startCase(this.fullname);
  next();
});

/* 
  Looks for a patient and if it does not exist, creates a new one.

  @param [String] fullname
  @param [String] email
  @param [String] phoneNumber
  @return [Patient]
*/
patientSchema.statics.findOrCreatePatient = async (fullname, email, phoneNumber) => {
  const existentPatient = await Patient.findOne({
    email: email
  });

  if (existentPatient) return existentPatient;

  const newPatient = await Patient.createPatient(fullname, email, phoneNumber);
  return newPatient;
}

/* 
  Creates a new patient.

  @param [String] fullname
  @param [String] email
  @param [String] phoneNumber
  @return [Patient]
*/
patientSchema.statics.createPatient = async (fullname, email, phoneNumber) => {
  const newPatient = new Patient({
    fullname: fullname,
    email: email,
    phoneNumber: phoneNumber
  })

  const patient = await newPatient.save();

  return patient;
}

const Patient = mongoose.model('Patient', patientSchema);

module.exports = Patient;