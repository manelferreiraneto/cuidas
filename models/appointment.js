const mongoose = require('mongoose');
const _ = require('lodash');
const moment = require('moment');
const Configuration = require('./configuration');
const ModelException = require('./utils/modelException');
const DateUtils = require('./utils/date');

const Schema = mongoose.Schema;

const appointmentSchema = new Schema({
  patientId: {
    type: Schema.ObjectId,
    required: true
  },
  details: {
    type: String,
  },
  status: {
    type: String,
    match: /^(created|cancelled)$/,
    default: 'created',
    required: true
  },
  scheduledForDate: {
    type: Date,
    required: true
  },
  expectedToEndAt: {
    type: Date,
    required: true
  },
  history: [{
    status: {
      type: String,
      match: /^(created|cancelled)$/,
      required: true
    },
    date: {
      type: Date,
      required: true
    }
  }]
});

appointmentSchema.pre('save', (next) => {
  this.fullname = _.startCase(this.fullname);
  next();
});

/* 
  Creates a new appointment.

  @param [ObjectId] patientId
  @param [String] details
  @param [String] date
  @param [Integer] duration: in minutes
  @return [Appointment]
*/
appointmentSchema.statics.createAppointment = async (patientId, details, date, duration) => {
  if (DateUtils.isWeekend(date)) 
    throw new ModelException(
      400,
      'You can only schedule an appointment on workdays'
    )

  const { canScheduleForTime, minTimeLimit, maxTimeLimit } = await Appointment.checkTimeConstraints(date);
  if (!canScheduleForTime)
    throw new ModelException(
      400,
      `You can only schedule an appointment for a time between ${minTimeLimit}h and ${maxTimeLimit}h`
    );
  
  const timeIsAvailable =  await Appointment.timeBlockIsAvailable(date, duration);
  if (!timeIsAvailable)
    throw new ModelException(
      400,
      `Oops... there is another appointment scheduled for the same time`
    );

  const scheduledForDate = moment(date, 'YYYY-MM-DD HH:mm ZZ');
  const expectedToEndAt = moment(scheduledForDate).add(duration, 'minute');

  const appointmentData = {
    patientId,
    scheduledForDate,
    expectedToEndAt,
    history: [{
      status: 'created',
      date: Date.now()
    }]
  }
  if (!_.isNil(details)) appointmentData.details = details;

  // const newAppointment = new Appointment(appointmentData);

  const appointment = await Appointment.create(appointmentData)

  // const appointment = await newAppointment.save();

  return appointment;
}

/* 
  Checks if an appointment can be scheduled for a given time.

  @param [String] date
  @return [JSON] containing [Bool] canScheduleForDate,
    [String] minTimeLimit and [String] maxTimeLimit
*/
appointmentSchema.statics.checkTimeConstraints = async (date) => {
  const appointmentTime = moment(date, 'YYYY-MM-DD HH:mm');

  const appointmentTimeLimits = await Configuration.findOne({
    name: 'appointmentTimeLimits'
  });
  const minTimeLimit = _.get(appointmentTimeLimits, 'value.min', '10:00')
  const maxTimeLimit = _.get(appointmentTimeLimits, 'value.max', '18:00')

  let startOfPeriod = moment(
    `${appointmentTime.format('YYYY-MM-DD')} ${minTimeLimit}`, 
    `YYYY-MM-DD HH:mm`
  );
  let endOfPeriod = moment(
    `${appointmentTime.format('YYYY-MM-DD')} ${maxTimeLimit}`, 
    `YYYY-MM-DD HH:mm`
  );

  return {
    canScheduleForTime: appointmentTime.isBetween(startOfPeriod, endOfPeriod, null, '[]'),
    minTimeLimit,
    maxTimeLimit
  }
}

/* 
  Lists the scheduled appointments

  @param [Integer] daysRange: number of days from the current day
  @return [Bool] 
*/
appointmentSchema.statics.getAppointments = async(daysRange = null) => {
  let match = {
    status: 'created'
  }
  const aggregateParams = [];
  if (daysRange && daysRange > 0) {
    const endOfPeriod = moment().set({
      hour: '23',
      minute: '59',
      second: '59',
    });
    endOfPeriod.add(daysRange, 'day');

    match = {
      ...match,
      scheduledForDate: {
        "$gte": new Date(),
        "$lte": new Date(endOfPeriod)
      }
    };
  }

  aggregateParams.push({
    $match: match
  });

  aggregateParams.push({
    $lookup: {
      from: 'patients',
      localField: 'patientId',
      foreignField: '_id',
      as: 'patient'
    }
  })

  const appointments = await Appointment.aggregate(
    aggregateParams
  ).sort('scheduledForDate').exec()

  return appointments;
}

/* 
  Checks if there is any appointments scheduled for a given block of time.

  @param [String] date
  @param [Integer] duration: in minutes
  @return [Bool] 
*/
appointmentSchema.statics.timeBlockIsAvailable = async(date, duration) => {
  const startOfPeriod = moment(date, 'YYYY-MM-DD HH:mm ZZ');
  const endOfPeriod = moment(startOfPeriod).add(duration, 'minute');

  const appointments = await Appointment.find({
    "$or": [
      {
        scheduledForDate: {
          "$lte": new Date(startOfPeriod)
        },
        expectedToEndAt: {
          "$gt": new Date(endOfPeriod)
        }
      },
      {
        scheduledForDate: {
          "$lte": new Date(startOfPeriod)
        },
        expectedToEndAt: {
          "$gt": new Date(startOfPeriod)
        }
      },
      {
        scheduledForDate: {
          "$lt": new Date(endOfPeriod)
        },
        expectedToEndAt: {
          "$gte": new Date(endOfPeriod)
        }
      }
    ],
    status: 'created'
  });

  return appointments.length === 0;
}

/* 
  Cancels an appointment.

  @param [Integer] id: one appointment id
  @return [Bool]
*/
appointmentSchema.statics.cancelAppointment = async(id) => {
  const cancelled = await Appointment.findOne({
    _id: id,
    status: 'created'
  })

  if (!cancelled)
    throw new ModelException(400, 'Appointment not found or was cancelled previously.')

  cancelled.status = 'cancelled';
  cancelled.history.push({
    status: 'cancelled',
    date: Date.now()
  });

  await cancelled.save();

  return cancelled;
}

/* 
  Returns available times (in minutes of the day) 
  for creating appointments in the next 15 days.

  @return [JSON]
*/
appointmentSchema.statics.getAvailableTimes = async() => {
  const { datesRange, totalNumberOfDays } = DateUtils.generateWorkDaysRange(15);

  let appointments = await Appointment.getAppointments(totalNumberOfDays);

  let defaultAvailableTimes = await getDefaultAvailableTimes();
  let defaultDurations = _.get(defaultAvailableTimes, 'durations');

  let groupedDates = {};
  appointments = _.each(appointments, (appointment) => {
    const appointmentDate = moment(appointment.scheduledForDate).format('YYYY-MM-DD');

    let availableTimes = _.get(groupedDates, appointmentDate, {...defaultAvailableTimes});

    const startOfAppointmentDay = moment(appointment.scheduledForDate).startOf('day');
    const appointmentStart = moment(appointment.scheduledForDate);
    const appointmentEnd = moment(appointment.expectedToEndAt);

    // minute of the day in which the appointment starts
    const appointmentStartMinute = DateUtils.differenceInMinutes(
      startOfAppointmentDay,
      appointmentStart
    ); 

    // duration of the appointment in minutes
    const durationInMinutes = DateUtils.differenceInMinutes(
      appointmentStart,
      appointmentEnd
    );
    
    // remove durations that are now invalid for previous times
    defaultDurations.forEach((duration) => {
      let previousTime = availableTimes[_.toString(appointmentStartMinute - duration)];
      if (!_.isNil(previousTime)) {
        let availableDurations = [...previousTime.durations]; 

        availableDurations = _.filter(availableDurations, (duration) => {
          return (previousTime.minuteOfDay + duration) <= appointmentStartMinute;
        });
    
        previousTime.durations = availableDurations;
      }
    });

    // remove time block of the current appointment
    for(let i = appointmentStartMinute; i < appointmentStartMinute + durationInMinutes; i = i + 10) {
      delete availableTimes[_.toString(i)];
    }

    groupedDates = {
      ...groupedDates,
      [appointmentDate]: availableTimes
    }
  });

  groupedDates = {
    ...groupedDates, 
    default: defaultAvailableTimes,
    datesRange
  }

  return groupedDates;
}

/* 
  Returns the default available times (in minutes of the day) 
  for creating an appointment.

  @return [Array]
*/
const getDefaultAvailableTimes = async () => {
  const { min, max } = await getAppointmentTimeLimits();
  let minTimeLimit = moment(min, 'HH:mm');
  let maxTimeLimit = moment(max, 'HH:mm');

  const startOfCurrentDay = moment().startOf('day');
  
  const startAtMinute = moment.duration(
    minTimeLimit.diff(startOfCurrentDay)
  ).asMinutes(); // number of minutes between midnight and min time limit

  const endAtMinute = moment.duration(
    maxTimeLimit.diff(startOfCurrentDay)
  ).asMinutes(); // number of minutes between midnight and max time limit
    
  let defaultAvailableTimes = {
    durations: [10, 20, 30]
  };
  for (let minute = startAtMinute; minute <= endAtMinute; minute = minute + 10) {
    var hh = Math.floor(minute / 60);
    var mm = (minute % 60);

    defaultAvailableTimes = {
      ...defaultAvailableTimes,
      [minute]: {
        minuteOfDay: minute,
        formatted: ("0" + (hh % 24)).slice(-2) + ':' + ("0" + mm).slice(-2),
        durations: [10, 20, 30]
      }
    }
  }

  return defaultAvailableTimes;
}

/* 
  Returns the time limits for creating an
  appointment.

  @return [JSON]
*/
const getAppointmentTimeLimits = async () => {
  const appointmentTimeLimits = await Configuration.findOne({
    name: 'appointmentTimeLimits'
  });
  const min = _.get(appointmentTimeLimits, 'value.min', '10:00');
  const max = _.get(appointmentTimeLimits, 'value.max', '18:00');

  return {
    min,
    max
  }
}

const Appointment = mongoose.model('Appointment', appointmentSchema);

module.exports = Appointment;