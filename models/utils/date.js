const moment = require('moment');
const _ = require('lodash');

/* 
  Returns a working days range for a given number of 
  calendar days. Also returns the total number of days.

  @param [Integer] numberOfDays
  @return [JSON]
*/
module.exports.generateWorkDaysRange = (numberOfDays = 15) => {
  const dates = [];
  let skippedDays = 0;

  if (numberOfDays < 1) numberOfDays = 15;
  
  for(let i = 1; i <= numberOfDays; i = i + 1) {
    let currentDay = moment().add(i + skippedDays, 'day')

    if (currentDay.weekday() === 0) {
      currentDay = currentDay.add(1, 'day');
      skippedDays = skippedDays + 1;
    } else if (currentDay.weekday() === 6) {
      currentDay = currentDay.add(2, 'day');
      skippedDays = skippedDays + 2;
    }

    dates.push(currentDay.format('YYYY-MM-DD'))
  }

  const firstDay = moment(_.first(dates), 'YYYY-MM-DD');
  const lastDay = moment(_.last(dates), 'YYYY-MM-DD');
  const totalNumberOfDays = moment.duration(
    lastDay.diff(firstDay)
  ).asDays();

  return {
    datesRange: dates,
    totalNumberOfDays
  }
}

/* 
  Returns the difference between two times in minutes.

  @param [String] startDate (ISO format)
  @param [String] endDate (ISO format)
  @return [Integer]
*/
module.exports.differenceInMinutes = (startDate, endDate) => {
  const start = moment(startDate);
  const end = moment(endDate);

  return moment.duration(
    end.diff(start)
  ).asMinutes();
}

/* 
  Checks if a date is a weekend day.

  @param [String] date
  @return [Boolean]
*/
module.exports.isWeekend = (date) => {
  const weekday = moment(date, 'YYYY-MM-DD HH:mm ZZ').weekday();
  return weekday === 0 || weekday === 6;
}