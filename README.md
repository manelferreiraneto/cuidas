# Cuidas API

## The API is deployed at

-> https://cuidas-api.herokuapp.com/

### Prerequisites

- NodeJS
- npm or yarn
- MongoDB

### Running the application

1) Install dependencies:
```
npm install
```

It's important to note that you need to set an enviroment variable with
the name MONGODB_HOST_URL poiting to your MongoDB instance.

2) Start the application:

```
npm start
```

### Running the tests

To run the tests locally use the following command:
```
npm test
```

### Authors

- **Manoel Ferreira Neto** - [Github](https://github.com/manelferreira)