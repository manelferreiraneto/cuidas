
const chai = require('chai');
const { expect } = chai;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const moment = require('moment');
const DateUtils = require('../../models/utils/date');
const Appointment = require('../../models/appointment');

chai.use(chaiAsPromised);

describe('Appointment Model', () => {
  describe('createAppointment', () => {
    context('when there is nothing wrong with the parameters', () => {
      beforeEach(() => {
        this.dateUtilsStub = sinon.stub(DateUtils, 'isWeekend').returns(false);
        this.checkTimeConstraintsStub = sinon.stub(Appointment, 'checkTimeConstraints').returns({
          canScheduleForTime: true
        });
        this.timeBlockIsAvailableStub = sinon.stub(Appointment, 'timeBlockIsAvailable').returns(true);
        this.createAppointmentStub = sinon.stub(Appointment, 'create').resolves('new appointment object');

        this.dateNowStub = sinon.stub(Date, 'now').returns('2019-01-01T10:00:00');
      });

      afterEach(() => {
        this.dateUtilsStub.restore();
        this.checkTimeConstraintsStub.restore();
        this.timeBlockIsAvailableStub.restore();
        this.createAppointmentStub.restore();
        this.dateNowStub.restore();
      });

      it('creates an appointment with no details', async () => {
        const duration = 10;
        const date = '2019-08-08 10:30'
        const scheduledForDate = moment(date, 'YYYY-MM-DD HH:mm ZZ');
        const expectedToEndAt = moment(scheduledForDate).add(duration, 'minute');

        const appointment = await Appointment.createAppointment(
          'some-patient-id',
          null,
          date,
          duration
        )

        expect(appointment).to.eq('new appointment object');

        sinon.assert.calledOnce(this.dateUtilsStub);
        sinon.assert.calledWith(this.dateUtilsStub, date);
        
        sinon.assert.calledOnce(this.checkTimeConstraintsStub);
        sinon.assert.calledWith(this.checkTimeConstraintsStub, date);

        sinon.assert.calledOnce(this.timeBlockIsAvailableStub);
        sinon.assert.calledWith(this.timeBlockIsAvailableStub, date, 10);

        sinon.assert.calledOnce(this.createAppointmentStub);
        sinon.assert.calledWith(this.createAppointmentStub, {
          patientId: 'some-patient-id',
          scheduledForDate,
          expectedToEndAt,
          history: [{
            status: 'created',
            date: '2019-01-01T10:00:00'
          }]
        });
      });

      it('creates an appointment with details', async () => {
        const duration = 10;
        const date = '2019-08-08 10:30'
        const scheduledForDate = moment(date, 'YYYY-MM-DD HH:mm ZZ');
        const expectedToEndAt = moment(scheduledForDate).add(duration, 'minute');

        const appointment = await Appointment.createAppointment(
          'some-patient-id',
          'some details',
          date,
          duration
        )

        expect(appointment).to.eq('new appointment object');

        sinon.assert.calledOnce(this.dateUtilsStub);
        sinon.assert.calledWith(this.dateUtilsStub, date);
        
        sinon.assert.calledOnce(this.checkTimeConstraintsStub);
        sinon.assert.calledWith(this.checkTimeConstraintsStub, date);

        sinon.assert.calledOnce(this.timeBlockIsAvailableStub);
        sinon.assert.calledWith(this.timeBlockIsAvailableStub, date, 10);

        sinon.assert.calledOnce(this.createAppointmentStub);
        sinon.assert.calledWith(this.createAppointmentStub, {
          patientId: 'some-patient-id',
          scheduledForDate,
          expectedToEndAt,
          history: [{
            status: 'created',
            date: '2019-01-01T10:00:00'
          }],
          details: 'some details'
        });
      });
    });

    context('when there is something wrong with the parameters', () => {
      beforeEach(() => {
        this.createAppointmentStub = sinon.stub(Appointment, 'create');
      });

      afterEach(() => {
        this.createAppointmentStub.restore();
      });

      context('when date is during a weekend', () => {
        beforeEach(() => {
          this.dateUtilsStub = sinon.stub(DateUtils, 'isWeekend').returns(true);
          this.checkTimeConstraintsStub = sinon.stub(Appointment, 'checkTimeConstraints').returns({
            canScheduleForTime: false
          });
          this.timeBlockIsAvailableStub = sinon.stub(Appointment, 'timeBlockIsAvailable').returns(false);
        });
  
        afterEach(() => {
          this.dateUtilsStub.restore();
          this.checkTimeConstraintsStub.restore();
          this.timeBlockIsAvailableStub.restore();
        });

        it('raises an exception', async () => {
          const duration = 10;
          const date = '2019-08-08 10:30'

          await expect(
            Appointment.createAppointment(
              'some-patient-id',
              null,
              date,
              duration
            )
          ).to.be.rejectedWith(
            'You can only schedule an appointment on workdays'
          );

  
          sinon.assert.calledOnce(this.dateUtilsStub);
          sinon.assert.calledWith(this.dateUtilsStub, date);
          
          sinon.assert.notCalled(this.checkTimeConstraintsStub);
          sinon.assert.notCalled(this.timeBlockIsAvailableStub);
          sinon.assert.notCalled(this.createAppointmentStub);
        });
      });

      context('when time is out of the constraints', () => {
        beforeEach(() => {
          this.dateUtilsStub = sinon.stub(DateUtils, 'isWeekend').returns(false);
          this.checkTimeConstraintsStub = sinon.stub(Appointment, 'checkTimeConstraints').returns({
            canScheduleForTime: false,
            minTimeLimit: '10:00',
            maxTimeLimit: '18:00'
          });
          this.timeBlockIsAvailableStub = sinon.stub(Appointment, 'timeBlockIsAvailable').returns(false);
        });
  
        afterEach(() => {
          this.dateUtilsStub.restore();
          this.checkTimeConstraintsStub.restore();
          this.timeBlockIsAvailableStub.restore();
        });

        it('raises an exception', async () => {
          const duration = 10;
          const date = '2019-08-08 10:30'

          await expect(
            Appointment.createAppointment(
              'some-patient-id',
              null,
              date,
              duration
            )
          ).to.be.rejectedWith(
            'You can only schedule an appointment for a time between 10:00h and 18:00h'
          );

  
          sinon.assert.calledOnce(this.dateUtilsStub);
          sinon.assert.calledWith(this.dateUtilsStub, date);
          
          sinon.assert.calledOnce(this.checkTimeConstraintsStub);
          sinon.assert.notCalled(this.timeBlockIsAvailableStub);
          sinon.assert.notCalled(this.createAppointmentStub);
        });
      });

      context('when time block is not available', () => {
        beforeEach(() => {
          this.dateUtilsStub = sinon.stub(DateUtils, 'isWeekend').returns(false);
          this.checkTimeConstraintsStub = sinon.stub(Appointment, 'checkTimeConstraints').returns({
            canScheduleForTime: true,
            minTimeLimit: '10:00',
            maxTimeLimit: '18:00'
          });
          this.timeBlockIsAvailableStub = sinon.stub(Appointment, 'timeBlockIsAvailable').returns(false);
        });
  
        afterEach(() => {
          this.dateUtilsStub.restore();
          this.checkTimeConstraintsStub.restore();
          this.timeBlockIsAvailableStub.restore();
        });

        it('raises an exception', async () => {
          const duration = 10;
          const date = '2019-08-08 10:30'

          await expect(
            Appointment.createAppointment(
              'some-patient-id',
              null,
              date,
              duration
            )
          ).to.be.rejectedWith(
            'Oops... there is another appointment scheduled for the same time'
          );

  
          sinon.assert.calledOnce(this.dateUtilsStub);
          sinon.assert.calledWith(this.dateUtilsStub, date);
          
          sinon.assert.calledOnce(this.checkTimeConstraintsStub);
          sinon.assert.calledOnce(this.timeBlockIsAvailableStub);

          sinon.assert.notCalled(this.createAppointmentStub);
        });
      });


    });
  });
});